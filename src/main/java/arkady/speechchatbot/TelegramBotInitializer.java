package arkady.speechchatbot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

@Configuration
public class TelegramBotInitializer {
    private static final Logger log = LoggerFactory.getLogger(TelegramBotInitializer.class);

    @Value("${telegramBot.name}")
    private String botName;

    @Value("${telegramBot.token}")
    private String botToken;

    @Value("${speechKit.key}")
    private String speechKey;

    public void initializeBot() {
        ApiContextInitializer.init();

        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

        try {
            telegramBotsApi.registerBot(new TelegramBot(botName, botToken, speechKey));
            log.info("Bot initialized! Waiting queries...");
        } catch (TelegramApiRequestException e) {
            log.warn("Failed to initialize main bot!", e);
        }
    }
}
