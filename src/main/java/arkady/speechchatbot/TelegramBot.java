package arkady.speechchatbot;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendVoice;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class TelegramBot extends TelegramLongPollingBot {
    private static final Logger log = LoggerFactory.getLogger(TelegramBot.class);

    private final static int MAX_STRING_LENGTH = 2000;

    private String botName;
    private String botToken;
    private String speechKey;

    TelegramBot(String botName, String botToken, String speechKey) {
        this.botName = botName;
        this.botToken = botToken;
        this.speechKey = speechKey;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            try {
                String text = update.getMessage().getText();

                final byte[] utf8Bytes = text.getBytes("UTF-8");

                if (utf8Bytes.length > MAX_STRING_LENGTH) {
                    SendMessage sendMessage = new SendMessage()
                            .setChatId(update.getMessage().getChatId())
                            .setText("Too big message!");
                    execute(sendMessage);
                }

                List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
                messageConverters.add(new ByteArrayHttpMessageConverter());

                RestTemplate restTemplate = new RestTemplate(messageConverters);

                URIBuilder builder = new URIBuilder()
                        .setScheme("https")
                        .setHost("tts.voicetech.yandex.net/generate")
                        .setParameter("text", text)
                        .setParameter("format", "opus")
                        .setParameter("lang", "ru-RU")
                        .setParameter("speaker", "zahar")
                        .setParameter("emotion", "neutral")
                        .setParameter("speed", "1")
                        .setParameter("key", speechKey);
                URI url = builder.build();

                ResponseEntity<byte[]> response = restTemplate.getForEntity(url, byte[].class);

                InputStream inputStream = new ByteArrayInputStream(response.getBody());

                SendVoice sendVoice = new SendVoice()
                        .setVoice(update.getMessage().getFrom().toString(), inputStream)
                        .setChatId(update.getMessage().getChatId());

                execute(sendVoice);
            } catch (TelegramApiException e) {
                log.warn("Failed to received update:\n{}", update, e);
            } catch (UnsupportedEncodingException | URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onUpdatesReceived(List<Update> updates) {
        if (updates.size() > 0) {
            onUpdateReceived(updates.get(0));
        }
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}
