package arkady.speechchatbot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableCaching
@EnableScheduling
public class SpeechChatBotApplication {

    @Autowired
    private static TelegramBotInitializer botInitializer;

    public SpeechChatBotApplication(TelegramBotInitializer botInitializer) {
        SpeechChatBotApplication.botInitializer = botInitializer;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpeechChatBotApplication.class, args);

        botInitializer.initializeBot();
    }
}
